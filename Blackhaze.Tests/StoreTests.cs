using System;
using Xunit;

namespace Blackhaze.Tests
{
    public class StoreTests : IDisposable
    {
        readonly Store store;

        public StoreTests()
        {
            store = Store.Initialize(new FakeLogger(), false, Guid.NewGuid().ToString(), true);
        }

        [Fact]
        public void StoreVersion()
        {
            store.CreateNewVersion();
            var version2 = store.CreateNewVersion();
            var version3 = store.CreateNewVersion();
            Assert.Equal(version3, store.Version);
            Assert.Equal(version2, store.PreviousVersion);
        }

        [Fact]
        public void SaveFileVersion_then_GetTouchingFileMetadata()
        {
            var touchId = Guid.NewGuid();
            var storedFile = new FileMetadata(Guid.NewGuid(), "a", 1, 100, 0, touchId, 2);
            var newStoredFile = new FileMetadata(Guid.NewGuid(), "a", 3, 100, 0, touchId, null);
            store.InsertFileVersion(storedFile);
            store.InsertFileVersion(newStoredFile);
            var fetchedFile = store.GetTouchingFileMetadata("a", touchId);
            Assert.Equal(newStoredFile, fetchedFile);
        }

        [Fact]
        public void BlockUploaded()
        {
            var fileId = Guid.NewGuid();
            var storeVersion = Guid.NewGuid();
            var touchId = Guid.NewGuid();
            var file = new FileMetadata(fileId, "a", 100, 1, BlockCount: 3);
            var block1 = new Block(1, new byte[1]);
            var blockMetadata1 = new BlockMetadata(storeVersion, block1.Checksum, "b");
            var block2 = new Block(2, new byte[2]);
            var blockMetadata2 = new BlockMetadata(storeVersion, block2.Checksum, "b");
            store.InsertFileVersion(file);
            store.ThreadSafe_BlockUploaded(file, block1, blockMetadata1);
            store.ThreadSafe_BlockUploaded(file, block2, blockMetadata2);
            var fetchedFile = store.GetTouchingFileMetadata("a", touchId);
            var expectedFile = file with { TouchId = touchId };
            Assert.Equal(expectedFile, fetchedFile);
            Assert.True(store.IsBlockUploaded(block1.Checksum));
            Assert.True(store.IsBlockUploaded(block2.Checksum));
        }

        [Fact]
        public void DeleteFilesWithBlocksLeft()
        {
            var touchId = Guid.NewGuid();
            var file1 = new FileMetadata(Guid.NewGuid(), "a", 100, 1, BlockCount: 0);
            var file2 = new FileMetadata(Guid.NewGuid(), "b", 100, 1, BlockCount: 1);
            store.InsertFileVersion(file1);
            store.InsertFileVersion(file2);

            store.DeleteFilesWithBlocksLeft();

            var fetchedFile1 = store.GetTouchingFileMetadata("a", touchId);
            var fetchedFile2 = store.GetTouchingFileMetadata("b", touchId);
            Assert.Equal(file1 with { TouchId = touchId }, fetchedFile1);
            Assert.Null(fetchedFile2);
        }

        [Fact]
        public void DeleteRetiredFiles()
        {
            var touchId = Guid.NewGuid();
            var file1 = new FileMetadata(Guid.NewGuid(), "a", 1, 100, BlockCount: 1, RetiredTicks: 10);
            store.InsertFileVersion(file1);
            var block = new Block(0, new byte[1]);
            store.ThreadSafe_BlockUploaded(file1, block, new BlockMetadata(Guid.Empty, block.Checksum, "asd"));

            store.DeleteRetiredFilesOlderThan(11);

            var retrievedFile = store.GetTouchingFileMetadata("a", touchId);
            Assert.Null(retrievedFile);
        }

        public void Dispose()
        {
            store.Dispose();
        }
    }
}
