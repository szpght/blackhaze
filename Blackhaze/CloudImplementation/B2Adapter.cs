﻿using B2Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Blackhaze.CloudImplementation
{
    class B2Adapter : ICloudAdapter
    {
        readonly ClientFactory clientFactory;

        B2Client Client => clientFactory.Create();

        public B2Adapter(ClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }

        public async Task<CloudFile> GetMetadata(string name)
        {
            var versions = await Client.Files.GetVersions(name, maxFileCount: 1);
            var file = versions.Files.SingleOrDefault();
            return CloudFile.FromB2File(file);
        }

        public async Task<CloudFile> UploadBlob(byte[] data, string name, Dictionary<string, string> fileInfo, CancellationToken cancellationToken) =>
            CloudFile.FromB2File(await Client.Files.Upload(data, name, fileInfo: fileInfo, cancelToken: cancellationToken));

        public async Task DeleteBlob(string fileId, string fileName) =>
            await Client.Files.Delete(fileId, fileName);

        public async Task<CloudFile> DownloadBlobByName(string name, string bucketName) =>
            CloudFile.FromB2File(await Client.Files.DownloadByName(name, bucketName));

        public async Task<CloudFile> DownloadBlobById(string id) =>
            CloudFile.FromB2File(await Client.Files.DownloadById(id));

        public async IAsyncEnumerable<CloudFile> GetFilesWithPrefix(string prefix)
        {
            var startFileName = "";
            while (startFileName != null)
            {
                var files = await Client.Files.GetListWithPrefixOrDemiliter(startFileName, prefix);
                foreach (var file in files.Files)
                {
                    yield return CloudFile.FromB2File(file);
                }

                startFileName = files.NextFileName;
            }
        }
    }
}
