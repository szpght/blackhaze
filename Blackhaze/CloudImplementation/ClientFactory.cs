﻿using B2Net;
using B2Net.Models;
using System;

namespace Blackhaze.CloudImplementation
{
    /// <summary>
    /// This class is a hack to speed up client creation using implementation details of B2.NET.
    /// </summary>
    class ClientFactory
    {
        readonly Logger logger;
        readonly string keyId;
        readonly string applicationKey;
        readonly string bucketId;
        readonly Lazy<B2AuthResponse> authResponse;

        public ClientFactory(Logger logger, Configuration configuration)
        {
            this.logger = logger;
            keyId = configuration.B2KeyId;
            applicationKey = configuration.B2ApplicationKey;
            bucketId = configuration.B2BucketId;
            authResponse = new(() => CreateAuthResponse());
        }

        public B2Client Create()
        {
            return new B2Client(CreateOptions(), authorizeOnInitialize: true);
        }

        B2Options CreateOptions()
        {
            var options = CreateInitialOptions();
            options.SetState(authResponse.Value);
            return options;
        }

        B2AuthResponse CreateAuthResponse()
        {
            logger.Info("Authenticating to B2");
            var options = CreateInitialOptions();
            new B2Client(options, authorizeOnInitialize: true);
            logger.Info("Authenticated to B2");

            var capabilities = options.Capabilities;
            return new B2AuthResponse
            {
                absoluteMinimumPartSize = options.AbsoluteMinimumPartSize,
                accountId = options.AccountId,
                apiUrl = options.ApiUrl,
                authorizationToken = options.AuthorizationToken,
                downloadUrl = options.DownloadUrl,
                recommendedPartSize = options.RecommendedPartSize,
                allowed = new B2AuthCapabilities
                {
                    bucketId = capabilities.BucketId,
                    bucketName = capabilities.BucketName,
                    capabilities = capabilities.Capabilities,
                    namePrefix = capabilities.NamePrefix,
                },
            };
        }

        B2Options CreateInitialOptions() =>
            new B2Options
            {
                KeyId = keyId,
                ApplicationKey = applicationKey,
                BucketId = bucketId,
                PersistBucket = true,
            };
    }
}
