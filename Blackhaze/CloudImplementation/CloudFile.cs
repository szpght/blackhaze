﻿using B2Net.Models;
using System.Collections.Generic;

namespace Blackhaze.CloudImplementation
{
    record CloudFile(string FileId, string FileName, Dictionary<string, string> Properties = null, byte[] Data = null)
    {
        public static CloudFile FromB2File(B2File file) =>
            new CloudFile(file.FileId, file.FileName, file.FileInfo, file.FileData);
    }
}
