﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Blackhaze.CloudImplementation
{
    class DiskAdapter : ICloudAdapter
    {
        readonly string dataDirectory;

        public DiskAdapter(string dataDirectory)
        {
            this.dataDirectory = dataDirectory;
        }

        public Task DeleteBlob(string fileId, string fileName)
        {
            throw new NotImplementedException();
        }

        public Task<CloudFile> DownloadBlobById(string id)
        {
            throw new NotImplementedException();
        }

        public Task<CloudFile> DownloadBlobByName(string name, string bucketName)
        {
            throw new NotImplementedException();
        }

        public IAsyncEnumerable<CloudFile> GetFilesWithPrefix(string prefix)
        {
            throw new NotImplementedException();
        }

        public Task<CloudFile> GetMetadata(string name)
        {
            throw new NotImplementedException();
        }

        public Task<CloudFile> UploadBlob(byte[] data, string name, Dictionary<string, string> fileInfo, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}
