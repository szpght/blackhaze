﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Blackhaze.CloudImplementation
{
    interface ICloudAdapter
    {
        Task<CloudFile> UploadBlob(byte[] data, string name, Dictionary<string, string> fileInfo, CancellationToken cancellationToken);
        Task DeleteBlob(string fileId, string fileName);
        Task<CloudFile> GetMetadata(string name);
        Task<CloudFile> DownloadBlobByName(string name, string bucketName);
        Task<CloudFile> DownloadBlobById(string id);
        IAsyncEnumerable<CloudFile> GetFilesWithPrefix(string prefix);
    }
}
