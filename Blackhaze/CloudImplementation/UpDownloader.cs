﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace Blackhaze.CloudImplementation
{
    class UpDownloader
    {
        readonly Logger logger;
        readonly ICloudAdapter cloudAdapter;
        readonly Configuration configuration;

        public UpDownloader(Logger logger, ICloudAdapter cloudAdapter, Configuration configuration)
        {
            this.logger = logger;
            this.cloudAdapter = cloudAdapter;
            this.configuration = configuration;
        }

        public async Task<CloudFile> UploadBlob(string name, byte[] data, ImmutableDictionary<string, string> properties = null, CancellationToken token = default)
        {
            using var input = new MemoryStream(data);
            var outputSize = data.Length + 16 - data.Length % 16;
            var outputBuffer = new byte[outputSize];
            using var output = new MemoryStream(outputBuffer);

            var aes = Aes.Create();
            aes.Key = configuration.EncryptionKey;
            using var encryptor = aes.CreateEncryptor();

            using var cryptoStream = new CryptoStream(input, encryptor, CryptoStreamMode.Read);
            cryptoStream.CopyTo(output);

            var fileInfo = new Dictionary<string, string>(properties ?? ImmutableDictionary<string, string>.Empty)
            {
                [Utils.IV_KEY] = Utils.SerializeIV(aes.IV)
            };

            while (true)
            {
                try
                {
                    return await this.cloudAdapter.UploadBlob(outputBuffer, name, fileInfo: fileInfo, token);
                }
                catch (Exception e)
                {
                    logger.Info("Exception in UploadBlob, waiting 5 seconds before retry");
                    logger.Info(e.ToString());
                    await Task.Delay(5000);
                }
            }
        }

        public async Task<(byte[] data, ImmutableDictionary<string, string> properties)> DownloadBlobByName(string name) =>
            HandleDownload(await cloudAdapter.DownloadBlobByName(name, configuration.B2BucketName));

        public async Task<(byte[] data, ImmutableDictionary<string, string> properties)> DownloadBlobById(string id) =>
            HandleDownload(await cloudAdapter.DownloadBlobById(id));

        public async Task<ImmutableDictionary<string, string>> GetFileProperties(string name)
        {
            var metadata = await cloudAdapter.GetMetadata(name);
            metadata.Properties.Remove(Utils.IV_KEY);
            return ImmutableDictionary.CreateRange(metadata.Properties);
        }

        (byte[] data, ImmutableDictionary<string, string> properties) HandleDownload(CloudFile file)
        {
            var data = DecryptB2File(file);
            file.Properties.Remove(Utils.IV_KEY);
            return (data, ImmutableDictionary.CreateRange(file.Properties));
        }

        public async Task DeleteByB2FileId(string b2FileId, string b2FileName) =>
            await cloudAdapter.DeleteBlob(b2FileId, b2FileName);

        public IAsyncEnumerable<CloudFile> GetFilesWithPrefix(string prefix) =>
            cloudAdapter.GetFilesWithPrefix(prefix);

        byte[] DecryptB2File(CloudFile file)
        {
            var aes = Aes.Create();
            aes.Key = configuration.EncryptionKey;
            aes.IV = Utils.DeserializeIV(file.Properties[Utils.IV_KEY]);
            using var decryptor = aes.CreateDecryptor();
            using var input = new MemoryStream(file.Data);
            using var cryptoStream = new CryptoStream(input, decryptor, CryptoStreamMode.Read);
            using var output = new MemoryStream();
            cryptoStream.CopyTo(output);
            return output.ToArray();
        }
    }
}
