﻿using System;
using System.Security.Cryptography;

namespace Blackhaze
{
    record Block(int Ordinal, byte[] Data)
    {
        public byte[] Checksum => checksum.Value;
        public Guid ShortChecksum => new Guid(Checksum.AsSpan(0, 16));

        readonly Lazy<byte[]> checksum = new Lazy<byte[]>(() =>
        {
            var buffer = new byte[64];
            SHA512.TryHashData(Data, buffer, out var bytesWritten);

            if (bytesWritten != 64)
            {
                throw new Exception("SHA512 sum doesn't have 64 bytes");
            }

            return buffer;
        });
    };
}
