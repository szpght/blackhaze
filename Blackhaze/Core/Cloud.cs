﻿using Blackhaze.CloudImplementation;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace Blackhaze
{
    class Cloud : IDisposable
    {
        readonly Logger logger;
        readonly Configuration configuration;
        readonly Store store;
        readonly SemaphoreSlim semaphore;
        readonly CancellationTokenSource cancellationTokenSource = new();
        readonly UpDownloader updown;

        public Cloud(Logger logger, Configuration configuration, Store store, ICloudAdapter cloudAdapter)
        {
            this.logger = logger;
            this.configuration = configuration;
            this.semaphore = new SemaphoreSlim(configuration.ParallelUploads);
            this.store = store;
            this.updown = new UpDownloader(logger, cloudAdapter, configuration);
        }

        public void Dispose()
        {
            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();
        }

        public async Task EnqueueUpload(FileMetadata fileMetadata, Block block)
        {
            await Enqueue(async () =>
            {
                logger.Info($"Uploading block {block.ShortChecksum}");
                var blockMetadata = new BlockMetadata(store.Version, block.Checksum);
                var result = await updown.UploadBlob(blockMetadata.CloudName, block.Data, token: cancellationTokenSource.Token);
                blockMetadata = blockMetadata with { B2FileId = result.FileId };
                store.ThreadSafe_BlockUploaded(fileMetadata, block, blockMetadata);
                logger.Info($"Block {block.ShortChecksum} uploaded");
            });
        }

        public async Task EnqueueDelete(BlockMetadata blockMetadata)
        {
            await Enqueue(async () =>
            {
                logger.Info($"Deleting file {blockMetadata.CloudName}");
                await updown.DeleteByB2FileId(blockMetadata.B2FileId, blockMetadata.CloudName);
                store.ThreadSafe_BlockDeleted(blockMetadata.Checksum);
                logger.Info($"File {blockMetadata.CloudName} deleted");
            });
        }

        public async Task WaitForConcurrentOperations()
        {
            // TODO better solution
            logger.Info("Waiting for concurrent operations to finish");
            for (int i = 0; i < configuration.ParallelUploads; i++)
            {
                await semaphore.WaitAsync();
            }
            semaphore.Release(configuration.ParallelUploads);
        }

        public async Task SyncStore()
        {
            var backupFile = "blackhaze.sqlite.bak";
            File.Delete(backupFile);
            store.Backup(backupFile);
            var backupFileContents = File.ReadAllBytes(backupFile);
            var properties = ImmutableDictionary.Create<string, string>()
                .Add(Utils.STORE_VERSION, store.Version.ToString());
            await updown.UploadBlob(Utils.STORE_FILE_NAME, backupFileContents, properties);
        }

        public async Task<Guid> GetSyncedStoreVersion()
        {
            var properties = await updown.GetFileProperties(Utils.STORE_FILE_NAME);
            if (properties.ContainsKey(Utils.STORE_VERSION))
            {
                return Guid.Parse(properties[Utils.STORE_VERSION]);
            }

            return Guid.Empty;
        }

        public async Task<Guid> GetIntentVersion() =>
            Utils.DeserializeIntentVersion((await updown.DownloadBlobByName(Utils.INTENT_VERSION)).data);

        public async Task SetIntentVersion(Guid version) =>
            await updown.UploadBlob(Utils.INTENT_VERSION, Utils.SerializeIntentVersion(version));

        public async Task<byte[]> GetStore() =>
            (await updown.DownloadBlobByName(Utils.STORE_FILE_NAME)).data;

        async Task Enqueue(Func<Task> action)
        {
            await semaphore.WaitAsync();
            _ = Task.Run(async () =>
            {
                try
                {
                    await action();
                }
                finally
                {
                    semaphore.Release();
                }
            });
        }

        public IAsyncEnumerable<CloudFile> GetFilesWithPrefix(string prefix) =>
            updown.GetFilesWithPrefix(prefix);
    }
}
