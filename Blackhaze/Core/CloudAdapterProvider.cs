﻿using Blackhaze.CloudImplementation;

namespace Blackhaze
{
    static class CloudAdapterProvider
    {
        public static ICloudAdapter Create(Logger logger, Configuration configuration)
        {
            if (configuration.UseDiskCloud)
            {
                return new DiskAdapter(configuration.DiskCloudDataDirectory);
            }
            else
            {
                var clientFactory = new ClientFactory(logger, configuration);
                return new B2Adapter(clientFactory);
            }
        }
    }
}
