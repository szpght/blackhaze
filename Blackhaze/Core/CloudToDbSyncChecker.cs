﻿using System.IO;
using System.Threading.Tasks;

namespace Blackhaze
{
    class CloudToDbSyncChecker
    {
        readonly Logger logger;
        readonly Store store;
        readonly Cloud cloud;

        public CloudToDbSyncChecker(Logger logger, Store store, Cloud cloud)
        {
            this.logger = logger;
            this.store = store;
            this.cloud = cloud;
        }

        public async Task BeforeBackup()
        {
            var syncedStoreVersion = await cloud.GetSyncedStoreVersion();
            logger.Info($"Last synchronized store version: {syncedStoreVersion}");

            var intentVersion = await cloud.GetIntentVersion();
            logger.Info($"Last intent version: {intentVersion}");

            var storeVersion = store.Version;
            logger.Info($"Local store version: {storeVersion}");

            var previousStoreVersion = store.PreviousVersion;
            logger.Info($"Previous local store version: {previousStoreVersion}");

            if (intentVersion == syncedStoreVersion)
            {
                // last backup finished cleanly

                if (intentVersion == storeVersion)
                {
                    logger.Info("Last backup finished cleanly");
                    await CreateStoreVersion();
                }
                else if(intentVersion == previousStoreVersion)
                {
                    logger.Info("Last backup interrupted after store version generation, skipping version generation");
                }
                else
                {
                    logger.Info("Unrecognized local store version");
                    await DropLocalStore();
                }
            }
            else
            {
                logger.Info("Last backup interrupted");
                if (intentVersion == storeVersion)
                {
                    await CleanInterruptedBackup();
                }
                else
                {
                    logger.Info("Local store version different from interrupted backup");
                    await DropLocalStore();
                    store.InsertVersion(intentVersion);
                    await CleanInterruptedBackup();
                }
            }
        }

        public async Task AfterBackup()
        {
            await cloud.SyncStore();
        }

        async Task CreateStoreVersion()
        {
            var version = store.CreateNewVersion();
            logger.Info($"New store version: {version}");

            logger.Info("Setting intent version");
            await cloud.SetIntentVersion(version);
        }

        async Task CleanInterruptedBackup()
        {
            logger.Info("Deleting unfinished files");
            store.DeleteFilesWithBlocksLeft();

            logger.Info("Retrieving block list from cloud");
            var files = cloud.GetFilesWithPrefix(store.Version.ToString());
            await foreach (var file in files)
            {
                var block = BlockMetadata.FromCloudName(file.FileName, file.FileId);
                store.InsertBlock(block);
            }
        }

        async Task DropLocalStore()
        {
            await store.DoWithDbFile(async dbFilePath =>
            {
                logger.Info("Dropping local store");
                File.Delete(dbFilePath);

                logger.Info("Retrieving store from cloud");
                var storeFromCloud = await cloud.GetStore();
                File.WriteAllBytes(dbFilePath, storeFromCloud);
            });
        }
    }
}
