﻿using Microsoft.Extensions.Configuration;
using System.Collections.Immutable;
using System.Globalization;
using System.Linq;
using System.Numerics;

namespace Blackhaze
{
    class Configuration
    {
        public string MachineName { get; }
        public ImmutableArray<(string, string)> BackedDirectories { get; }
        public ImmutableArray<string> AlwaysCheckedFilePathsUpper { get; }
        public string B2KeyId { get; }
        public string B2ApplicationKey { get; }
        public string B2BucketId { get; }
        public string B2BucketName { get; }
        public int ParallelUploads { get; }
        public int BlockSize { get; }
        public byte[] EncryptionKey { get; }
        public int DeleteOldVersionsAfterDays { get; }
        public bool StoreGuidsAsBlobs { get; }
        public bool UseDiskCloud { get; }
        public string DiskCloudDataDirectory { get; }

        public Configuration(IConfigurationRoot configuration)
        {
            B2ApplicationKey = configuration[nameof(B2ApplicationKey)];
            B2KeyId = configuration[nameof(B2KeyId)];
            B2BucketId = configuration[nameof(B2BucketId)];
            B2BucketName = configuration[nameof(B2BucketName)];
            BlockSize = int.Parse(configuration[nameof(BlockSize)]);
            MachineName = configuration[nameof(MachineName)];
            ParallelUploads = int.Parse(configuration[nameof(ParallelUploads)]);
            AlwaysCheckedFilePathsUpper = GetSection(configuration, "AlwaysCheckedFiles")
                .Select(x => x.Item2.ToUpper())
                .ToImmutableArray();
            BackedDirectories = GetSection(configuration, nameof(BackedDirectories));
            EncryptionKey = BigInteger.Parse(configuration[nameof(EncryptionKey)], NumberStyles.HexNumber)
                .ToByteArray(isBigEndian: true);
            DeleteOldVersionsAfterDays = int.Parse(configuration[nameof(DeleteOldVersionsAfterDays)]);
            StoreGuidsAsBlobs = bool.Parse(configuration[nameof(StoreGuidsAsBlobs)]);
            UseDiskCloud = bool.Parse(configuration[nameof(UseDiskCloud)]);
            DiskCloudDataDirectory = configuration[nameof(DiskCloudDataDirectory)];
        }

        public static Configuration Provide()
        {
            var configuration = new ConfigurationBuilder()
                .AddIniFile("blackhaze.ini", false, false)
                .Build();
            return new Configuration(configuration);
        }

        static ImmutableArray<(string, string)> GetSection(IConfigurationRoot configuration, string name) =>
            configuration
                .GetSection(name)
                .GetChildren()
                .Select(x => (x.Key, x.Value))
                .ToImmutableArray();
    }
}
