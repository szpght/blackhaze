﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Blackhaze
{
    class FileReader
    {
        readonly int blockSize;
        readonly PathConverter pathConverter;

        public FileReader(Configuration configuration, PathConverter pathConverter)
        {
            this.blockSize = configuration.BlockSize;
            this.pathConverter = pathConverter;
        }

        public IEnumerable<Block> ReadBlocksLazy(string absolutePath)
        {
            var snapshotPath = pathConverter.AbsolutePathToSnapshotPath(absolutePath);
            using var stream = File.OpenRead(snapshotPath);
            int dataRead = 0;
            int currentBlock = 0;
            byte[] buffer = new byte[blockSize];
            while ((dataRead = stream.Read(buffer.AsSpan())) > 0)
            {
                if (dataRead == buffer.Length)
                {
                    yield return new Block(currentBlock, buffer);
                    buffer = new byte[blockSize];
                }
                else
                {
                    var data = new byte[dataRead];
                    buffer.AsSpan(0, dataRead).CopyTo(data);
                    yield return new Block(currentBlock, data);
                }
                currentBlock += 1;
            }
        }
    }
}
