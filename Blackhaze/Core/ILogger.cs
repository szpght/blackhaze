﻿namespace Blackhaze
{
    interface ILogger
    {
        void Info(string message);
    }
}
