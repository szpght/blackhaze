﻿using System;

namespace Blackhaze
{
    class Logger : ILogger
    {
        public void Info(string message) => Console.WriteLine(message);
    }
}
