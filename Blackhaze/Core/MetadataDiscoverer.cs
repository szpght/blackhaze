﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Blackhaze
{
    class MetadataDiscoverer
    {
        readonly Configuration configuration;
        readonly PathConverter pathConverter;

        public MetadataDiscoverer(Configuration configuration, PathConverter pathConverter)
        {
            this.configuration = configuration;
            this.pathConverter = pathConverter;
        }

        public IEnumerable<FileMetadata> Discover(Guid storeVersion) =>
            configuration.BackedDirectories
                .SelectMany(x => ScanDirectory(x.Item2, storeVersion));

        IEnumerable<FileMetadata> ScanDirectory(string absolutePath, Guid storeVersion) =>
            Directory.EnumerateFiles(pathConverter.AbsolutePathToSnapshotPath(absolutePath), "*", SearchOption.AllDirectories)
                .Select(x => new FileInfo(x))
                .Select(fileInfo => new FileMetadata(
                    Guid.Empty,
                    pathConverter.SnapshotPathToAbsolutePath(fileInfo.FullName),
                    fileInfo.LastWriteTimeUtc.Ticks,
                    fileInfo.Length,
                    GetBlockCount(fileInfo),
                    storeVersion,
                    null));

        long GetBlockCount(FileInfo fileInfo)
        {
            var n = fileInfo.Length / configuration.BlockSize;
            if (fileInfo.Length % configuration.BlockSize != 0)
            {
                n += 1;
            }

            return n;
        }
    }
}
