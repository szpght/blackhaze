﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackhaze
{
    class Orchestrator
    {
        public async Task PerformBackup()
        {
            try
            {
                await PerformBackupInternal();
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        async Task PerformBackupInternal()
        {
            var logger = new Logger();
            var configuration = Configuration.Provide();
            using var store = Store.Initialize(logger, configuration.StoreGuidsAsBlobs);
            var cloudAdapter = CloudAdapterProvider.Create(logger, configuration);
            using var cloud = new Cloud(logger, configuration, store, cloudAdapter);
            var syncChecker = new CloudToDbSyncChecker(logger, store, cloud);
            await syncChecker.BeforeBackup();
            using var snapshot = Snapshot.Create(configuration);
            // get backup time after creating snapshot when we know that no file will be updated
            var backupTime = DateTime.UtcNow;
            var storeVersion = store.Version;
            var pathConverter = new PathConverter(snapshot.DriveToPathPrefixMap);
            var fileReader = new FileReader(configuration, pathConverter);
            var metadataDiscoverer = new MetadataDiscoverer(configuration, pathConverter);
            var metadata = metadataDiscoverer.Discover(storeVersion);
            var metadataToUpload = new List<FileMetadata>();
            foreach (var currentMetadata in metadata)
            {
                var savedMetadata = store.GetTouchingFileMetadata(currentMetadata.Path, storeVersion);
                var isAlwaysCheckedFile = configuration.AlwaysCheckedFilePathsUpper
                    .Contains(currentMetadata.Path.ToUpper());
                var shouldUpload = savedMetadata == null
                    || currentMetadata.LastModifiedTicks > savedMetadata.LastModifiedTicks
                    || isAlwaysCheckedFile;
                if (shouldUpload)
                {
                    var newMetadata = currentMetadata with
                    {
                        Id = Guid.NewGuid(),
                        TouchId = storeVersion,
                        LastModifiedTicks = isAlwaysCheckedFile ? backupTime.Ticks : currentMetadata.LastModifiedTicks,
                    };

                    metadataToUpload.Add(newMetadata);
                }
            }

            logger.Info($"Discovered {metadataToUpload.Count} files to upload");

            foreach (var newMetadata in metadataToUpload)
            {
                store.InsertFileVersion(newMetadata);

                var blocks = fileReader.ReadBlocksLazy(newMetadata.Path);
                foreach (var block in blocks)
                {
                    if (store.IsBlockUploaded(block.Checksum))
                    {
                        store.InsertBlockForFile(newMetadata.Id, block.Ordinal, block.Checksum);
                    }
                    else
                    {
                        await cloud.EnqueueUpload(newMetadata, block);
                    }
                }
            }

            await cloud.WaitForConcurrentOperations();
            store.MarkUntouchedFilesAsRetired(storeVersion, backupTime.Ticks);
            store.DeleteRetiredFilesOlderThan(backupTime.AddDays(-configuration.DeleteOldVersionsAfterDays).Ticks);
            var orphanedBlocks = store.GetOrphanedBlocks();
            foreach (var block in orphanedBlocks)
            {
                await cloud.EnqueueDelete(block);
            }

            await cloud.WaitForConcurrentOperations();
            await syncChecker.AfterBackup();
        }
    }
}
