﻿using System;
using System.Collections.Immutable;

namespace Blackhaze
{
    class PathConverter
    {
        readonly ImmutableDictionary<string, string> driveToPathPrefixMap;

        public PathConverter(ImmutableDictionary<string, string> driveToPathPrefixMap)
        {
            this.driveToPathPrefixMap = driveToPathPrefixMap;
        }

        public string SnapshotPathToAbsolutePath(string snapshotPath)
        {
            foreach (var xd in driveToPathPrefixMap)
            {
                if (snapshotPath.StartsWith(xd.Value))
                {
                    return xd.Key + snapshotPath.Substring(xd.Value.Length);
                }
            }

            throw new Exception($"No path prefix in the dictionary matches the snapshot path: {snapshotPath}");
        }

        public string AbsolutePathToSnapshotPath(string absolutePath)
        {
            var drive = Utils.GetDriveFromPath(absolutePath);
            var pathPrefix = driveToPathPrefixMap[drive];
            return pathPrefix + absolutePath.Substring(3);
        }
    }
}
