﻿using Alphaleonis.Win32.Vss;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace Blackhaze
{
    class Snapshot : IDisposable
    {
        public ImmutableDictionary<string, string> DriveToPathPrefixMap { get; }
        readonly IVssBackupComponents backup;

        public static Snapshot Create(Configuration configuration)
        {
            IVssBackupComponents backup = null;
            try
            {
                var directoryToDrivePairs = configuration.BackedDirectories
                    .Select(x => KeyValuePair.Create(x.Item2, Utils.GetDriveFromPath(x.Item2)));
                var directoryToDriveMap = ImmutableDictionary.CreateRange(directoryToDrivePairs);
                var drives = directoryToDriveMap.Select(x => x.Value).Distinct().ToImmutableArray();
                var driveToSnapshotIdMap = ImmutableDictionary.Create<string, Guid>();

                var vssFactory = VssFactoryProvider.Default.GetVssFactory();
                backup = vssFactory.CreateVssBackupComponents();
                backup.InitializeForBackup(null);
                backup.SetBackupState(false, false, VssBackupType.Full, false);
                backup.SetContext(VssSnapshotContext.FileShareBackup);
                backup.StartSnapshotSet();

                foreach (var drive in drives)
                {
                    var shadowCopyId = backup.AddToSnapshotSet(drive);
                    driveToSnapshotIdMap = driveToSnapshotIdMap.Add(drive, shadowCopyId);
                }

                backup.DoSnapshotSet();

                var driveToPathPrefixMap = ImmutableDictionary.Create<string, string>();
                foreach (var drive in drives)
                {
                    var snapshotId = driveToSnapshotIdMap[drive];
                    var pathPrefix = backup.GetSnapshotProperties(snapshotId).SnapshotDeviceObject;
                    if (!pathPrefix.EndsWith('\\'))
                    {
                        pathPrefix = pathPrefix + '\\';
                    }

                    driveToPathPrefixMap = driveToPathPrefixMap.Add(drive, pathPrefix);
                }

                return new Snapshot(backup, driveToPathPrefixMap);
            }
            catch
            {
                backup?.Dispose();
                throw;
            }
        }

        public Snapshot(IVssBackupComponents backup, ImmutableDictionary<string, string> driveToPathPrefixMap)
        {
            this.backup = backup;
            this.DriveToPathPrefixMap = driveToPathPrefixMap;
        }

        public void Dispose()
        {
            backup?.Dispose();
        }
    }
}
