﻿using Blackhaze.StoreImplementation;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackhaze
{
    class Store : IDisposable
    {
        Db db;
        readonly ILogger logger;
        readonly string dataSource;
        readonly bool inMemory;

        public Store(ILogger logger, Db db, string dataSource, bool inMemory)
        {
            this.db = db;
            this.logger = logger;
            this.dataSource = dataSource;
            this.inMemory = inMemory;
        }

        public static Store Initialize(
            ILogger logger,
            bool storeGuidsAsBlobs,
            string dataSource = "blackhaze.sqlite",
            bool inMemory = false)
        {
            DapperConfigurator.Configure(storeGuidsAsBlobs);
            var db = new Db(dataSource, inMemory);
            DbInitializer.Initialize(db);
            return new Store(logger, db, dataSource, inMemory);
        }

        const string SelectVersionCommon = "select Version from Versions order by Id desc ";

        public Guid Version =>
            db.Connection.QuerySingleOrDefault<Guid>(SelectVersionCommon + "limit 1");

        public Guid PreviousVersion =>
            db.Connection.QuerySingleOrDefault<Guid>(SelectVersionCommon + "limit 1 offset 1");

        public Guid CreateNewVersion()
        {
            var version = Guid.NewGuid();
            InsertVersion(version);
            return version;
        }

        public void InsertVersion(Guid version) =>
            db.Connection.Execute("insert into Versions(Version) values(@version)", new { version });

        public FileMetadata GetTouchingFileMetadata(string path, Guid touchId)
        {
            return db.Connection.QuerySingleOrDefault<FileMetadata>(
                "update Files set TouchId=@touchId where Path=@path and RetiredTicks is null;" +
                "select Id, Path, LastModifiedTicks, Size, BlockCount, TouchId, RetiredTicks from Files " +
                "where Path=@path order by LastModifiedTicks desc limit 1;",
                new { path, touchId });
        }

        public bool IsBlockUploaded(byte[] checksum) =>
            db.Connection.QuerySingle<int>(
                $"select exists(select 1 from Blocks where Checksum=@checksum)", new { checksum }) == 1;

        public void ThreadSafe_BlockUploaded(FileMetadata fileMetadata, Block block, BlockMetadata blockMetadata)
        {
            using var db = GetDb();
            using var transaction = db.Connection.BeginTransaction();
            InsertBlockInternal(db, blockMetadata);
            InsertBlockForFileInternal(db, fileMetadata.Id, block.Ordinal, block.Checksum);
            transaction.Commit();
        }

        public void ThreadSafe_BlockDeleted(byte[] checksum)
        {
            using var db = GetDb();
            db.Connection.Execute("delete from Blocks where Checksum=@checksum", new { checksum });
        }

        public void InsertFileVersion(FileMetadata file) =>
            db.Connection.Execute(
                "insert into Files(Id, Path, LastModifiedTicks, Size, TouchId, RetiredTicks, BlockCount) " +
                "values(@Id, @Path, @LastModifiedTicks, @Size, @TouchId, @RetiredTicks, @BlockCount)",
                file);

        public void InsertBlockForFile(Guid fileId, long ordinal, byte[] checksum) =>
            InsertBlockForFileInternal(db, fileId, ordinal, checksum);

        public void InsertBlock(BlockMetadata blockMetadata) =>
            InsertBlockInternal(db, blockMetadata);

        void InsertBlockInternal(Db db, BlockMetadata blockMetadata) =>
            db.Connection.Execute(
                "insert or ignore into Blocks(StoreVersion, Checksum, B2FileId) VALUES(@StoreVersion, @Checksum, @B2FileId)",
                blockMetadata);

        private void InsertBlockForFileInternal(Db db, Guid fileId, long ordinal, byte[] checksum) =>
            db.Connection.Execute(
                "insert into FileBlocks(FileId, Ordinal, Checksum) " +
                "values(@fileId, @ordinal, @checksum)",
                new { fileId, ordinal, checksum });

        public void MarkUntouchedFilesAsRetired(Guid touchId, long backupTimeTicks) =>
            db.Connection.Execute(
                "update Files set RetiredTicks=@backupTimeTicks, TouchId=null where TouchId != @touchId",
                new { touchId, backupTimeTicks });

        public void DeleteRetiredFilesOlderThan(long retiredTicks) =>
            db.Connection.Execute(
                "delete from Files where RetiredTicks < @retiredTicks",
                new { retiredTicks });

        public IEnumerable<BlockMetadata> GetOrphanedBlocks()
        {
            int batch = 1;
            IEnumerable<List<BlockMetadata>> getBatch()
            {
                logger.Info("Getting next batch of orphaned blocks");
                var results = db.Connection.Query<BlockMetadata>(
                    "select b.StoreVersion, b.Checksum, b.B2FileId from Blocks as b " +
                    "where not exists (select 1 from FileBlocks as fb where fb.Checksum = b.Checksum) " +
                    "limit 1000")
                    .AsList();

                if (results.Any())
                {
                    logger.Info($"Got batch {batch} of orphaned blocks");
                    batch += 1;
                    yield return results;
                }
                else
                {
                    logger.Info("No more batches");
                }
            };

            return getBatch().SelectMany(x => x);
        }

        public void DeleteFilesWithBlocksLeft()
        {
            const string subquery =
                "(select Id from Files as f " +
                "left join FileBlocks as fb on fb.FileId = f.Id " +
                "group by f.Id, fb.FileId " +
                "having count(fb.FileId) <> f.BlockCount)";
            db.Connection.Execute("delete from Files where Id in " + subquery);
        }

        public void Backup(string fileName) =>
            db.Backup(fileName);

        string GetKeyValue(string key) =>
            db.Connection.QuerySingle<string>("select Value from KeyValues where Id = @key", new { key });

        void SetKeyValue(string key, string value) =>
            db.Connection.Execute(
                "insert into KeyValues(Id, Value) values (@key, @value) " +
                "on conflict(Id) do " +
                "update set Value=@value where Id=@key",
                new { key, value });

        Db GetDb() =>
            new Db(dataSource, inMemory);

        public void Dispose() =>
            db.Dispose();

        public async Task DoWithDbFile(Func<string, Task> action)
        {
            db.Connection.Close();
            db.Dispose();
            db = null;
            await action(dataSource);
            db = GetDb();
        }
    }
}
