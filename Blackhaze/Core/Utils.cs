﻿using System;

namespace Blackhaze
{
    static class Utils
    {
        public const string IV_KEY = "iv";
        public const string STORE_VERSION = "store-version";
        public const string STORE_FILE_NAME = "_STORE";
        public const string INTENT_VERSION = "_INTENT_VERSION";

        public static string SerializeIV(byte[] iv) =>
            Convert.ToBase64String(iv);

        public static byte[] DeserializeIV(string iv) =>
            Convert.FromBase64String(iv);

        public static string SerializeChecksum(byte[] checksum) =>
            Convert.ToBase64String(checksum).Replace('/', '-');

        public static byte[] DeserializeChecksum(string checksum) =>
            Convert.FromBase64String(checksum.Replace('-', '/'));

        public static byte[] SerializeIntentVersion(Guid version) =>
            version.ToByteArray();

        public static Guid DeserializeIntentVersion(byte[] data) =>
            new Guid(data);

        public static string GetDriveFromPath(string path) =>
            path.Substring(0, 3).ToUpper();
    }
}
