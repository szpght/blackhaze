﻿using System;

namespace Blackhaze
{
    record BlockMetadata
    (
        Guid StoreVersion,
        byte[] Checksum,
        string B2FileId = null
    )
    {
        public string CloudName => $"{StoreVersion}/{Utils.SerializeChecksum(Checksum)}";

        public static BlockMetadata FromCloudName(string cloudName, string b2FileId)
        {
            var split = cloudName.Split('/');
            var storeVersion = Guid.Parse(split[0]);
            var checksum = Utils.DeserializeChecksum(split[1]);
            return new BlockMetadata(storeVersion, checksum, b2FileId);
        }
    };
}
