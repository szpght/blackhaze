﻿using System;

namespace Blackhaze
{
    record FileBlockMetadata
    (
        Guid FileId,
        int Ordinal,
        byte[] Checksum
    );
}
