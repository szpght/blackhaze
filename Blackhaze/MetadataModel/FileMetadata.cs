﻿using System;

namespace Blackhaze
{
    record FileMetadata
    (
        Guid Id,
        string Path,
        long LastModifiedTicks,
        long Size,
        long BlockCount,
        Guid? TouchId = default,
        long? RetiredTicks = null
    );
}
