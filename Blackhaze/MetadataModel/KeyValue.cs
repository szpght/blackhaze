﻿namespace Blackhaze
{
    record KeyValue (string Id, string Value);
}
