﻿using System.Runtime.CompilerServices;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("Blackhaze.Tests")]

namespace Blackhaze
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await new Orchestrator().PerformBackup();
        }
    }
}
