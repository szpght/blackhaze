﻿using Dapper;
using System;
using System.Data;

namespace Blackhaze.StoreImplementation
{
    static class DapperConfigurator
    {
        public static void Configure(bool storeGuidsAsBlobs)
        {
            SqlMapper.RemoveTypeMap(typeof(Guid));
            SqlMapper.RemoveTypeMap(typeof(Guid?));
            SqlMapper.AddTypeHandler<Guid>(
                storeGuidsAsBlobs ? new GuidToBlob() : new GuidToString());
        }

        class GuidToBlob : SqlMapper.TypeHandler<Guid>
        {
            public override void SetValue(IDbDataParameter parameter, Guid guid) =>
                parameter.Value = guid.ToByteArray();

            public override Guid Parse(object value) =>
                new Guid((byte[])value);
        }

        class GuidToString : SqlMapper.TypeHandler<Guid>
        {
            public override void SetValue(IDbDataParameter parameter, Guid guid) =>
                parameter.Value = guid.ToString();

            public override Guid Parse(object value) =>
                Guid.Parse((string)value);
        }
    }
}
