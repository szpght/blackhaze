﻿using Dapper;
using Microsoft.Data.Sqlite;
using System;

namespace Blackhaze.StoreImplementation
{
    public class Db : IDisposable
    {
        public SqliteConnection Connection { get; }

        public Db(string dataSource, bool inMemory)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder();
            connectionStringBuilder.DataSource = dataSource;
            connectionStringBuilder.Cache = SqliteCacheMode.Shared;
            if (inMemory)
            {
                connectionStringBuilder.Mode = SqliteOpenMode.Memory;
            }

            Connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
            Connection.Open();
            Connection.Execute("PRAGMA foreign_keys = ON;");
            //connection.Execute("PRAGMA journal_mode = WAL;");
        }

        public void Backup(string fileName)
        {
            var builder = new SqliteConnectionStringBuilder();
            builder.DataSource = fileName;
            using var backup = new SqliteConnection(builder.ConnectionString);
            Connection.BackupDatabase(backup);
            backup.Close();
        }

        public void Dispose()
        {
            Connection.Dispose();
        }
    }
}
