﻿using System.IO;
using Dapper;

namespace Blackhaze.StoreImplementation
{
    static class DbInitializer
    {
        public static void Initialize(Db db)
        {
            var schema = StringFromResource($"Blackhaze.{nameof(StoreImplementation)}.schema.txt");
            db.Connection.Execute(schema);
        }

        static string StringFromResource(string resourceName)
        {
            using var schemaResource = typeof(DbInitializer)
                .Assembly
                .GetManifestResourceStream(resourceName);
            return new StreamReader(schemaResource)
                .ReadToEnd()
                .Trim();
        }
    }
}
